import React, { Component } from 'react';
import styled from 'styled-components';

const  StyledListItem = styled.li`
  padding-right: 33px;
  border-right: 1px dotted white;
  &:last-child {
    border-right:none;
  }
`

const  StyledLink = styled.a`
  color: ${props => props.active ? "pink" : "white"};
  font-size: 14px;
  font-weight: 400;
  line-height: 24px;

  
  &:hover {
    color: #8d8d8d;
    text-decoration: none;
  }
  &:focus {
    color: #8d8d8d;
    text-decoration: none;
  }
  &:active {
    color: #8d8d8d;
    text-decoration: none;
  }
`
class Link extends Component {
  render() {
    return (
        <StyledListItem>
          <StyledLink href={this.props.link}>{this.props.text}</StyledLink>
        </StyledListItem>
    );
  }
}

export default Link;